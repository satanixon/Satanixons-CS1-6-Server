# CSBase
A Counter-Strike 1.6 server base to the standards in the 2022

WARNING: Don't use this if you are going to use Windows, this is only for Linux.

# Content
This is the list of content that includes, modules, binaries, and more.

ReHLDS v3.11.0.779

ReGameDLL v5.21.0.556

Metamod-R v1.3.0.131

AMXX v1.9 - build 5294

ReAPI v5.21.0.252

Reunion v0.1.0.133

Revoice v0.1.0.33

SafeNameAndChat v1.1
